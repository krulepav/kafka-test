package cz.krulec.clustertest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import org.apache.kafka.clients.admin.NewTopic
import org.springframework.kafka.core.KafkaAdmin
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG
import org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.kafka.core.ProducerFactory


@SpringBootApplication
@EnableScheduling
class ClusterTestApplication

fun main(args: Array<String>) {
    runApplication<ClusterTestApplication>(*args)
}

//@Configuration
//class KafkaTopicConfig {
//
//    @Value(value = "\${spring.kafka.bootstrap-servers}")
//    private lateinit var bootstrapAddress: String
//
//    @Bean
//    fun kafkaAdmin(): KafkaAdmin {
//        val configs = mapOf(Pair(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress))
//        return KafkaAdmin(configs)
//    }
//
//    @Bean
//    fun topic1(): NewTopic {
//        return NewTopic("clusterTestTopic", 1, 1.toShort())
//    }
//}
//
//@Configuration
//class KafkaProducerConfig {
//
//    @Value(value = "\${spring.kafka.bootstrap-servers}")
//    private lateinit var bootstrapAddress: String
//
//    @Bean
//    fun producerFactory(): ProducerFactory<String, String> {
//        val configProps = mapOf(
//                Pair(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress),
//                Pair(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java),
//                Pair(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
//        )
//        return DefaultKafkaProducerFactory(configProps)
//    }
//
//    @Bean
//    fun kafkaTemplate(): KafkaTemplate<String, String> {
//        return KafkaTemplate(producerFactory())
//    }
//}
