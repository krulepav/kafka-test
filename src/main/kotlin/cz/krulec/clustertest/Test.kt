package cz.krulec.clustertest

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.support.SendResult
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.util.concurrent.ListenableFutureCallback
import java.util.concurrent.ThreadLocalRandom


@Component
class Test(private val kafkaTemplate: KafkaTemplate<String, String>) {
    private val LOG = LoggerFactory.getLogger(Test::class.java)
    private val random = ThreadLocalRandom.current()

    @Value("\${name}")
    private lateinit var name: String

    @Scheduled(fixedDelay = 1000)
    fun scheduled() {
        val sleep = random.nextInt(0, 5)
        Thread.sleep(sleep * 1000L)
        LOG.info("I, $name, sent with sleep $sleep")
        kafkaTemplate.send("names", "$name fired with sleep $sleep").addCallback(object : ListenableFutureCallback<SendResult<String, String>> {
            override fun onSuccess(result: SendResult<String, String>?) {
                LOG.info("I, $name, delivered")
            }

            override fun onFailure(e: Throwable) {
                LOG.info("I, $name, failed ${e.message}")
                e.printStackTrace()
            }
        })
    }
}
